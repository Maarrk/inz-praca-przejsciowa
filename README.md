# inz-praca-przejsciowa

Praca przejściowa inżynierska realizowana w semestrze letnim 2018/2019.

Zakład: [Automatyki i Osprzętu Lotniczego](https://www.meil.pw.edu.pl/zaiol/ZAiOL/)

Kierujący pracą: [dr inż. Przemysław Bibik](https://www.meil.pw.edu.pl/zaiol/ZAiOL/Pracownicy2/Przemyslaw-Bibik)

Wstępny tytuł pracy: "System unikania kolizji dla bezzałogowego wielowirnikowca z wykorzystaniem metody sztucznych pól potencjalnych"
